const Logic = require('./BasicLogic')
module.exports = class extends Logic {
  description = 'Операция возведения в степень. Результ: y = Math.pow(x1, x2)'
  countY (c, n) { return Math.pow(c, n) }
}
