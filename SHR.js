const Logic = require('./BasicLogic')
module.exports = class extends Logic {
  description = 'Побитовый логический сдвиг вправо. Результ:  y = x1 >> x2'
  countY (c, n) { return c << n }
}
