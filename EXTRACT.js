const { Port, Rule, Device } = require('vrack-core')
module.exports = class extends Device {
  ports () {
    return [
      new Port('x%d').input().data('number')
        .dynamic(2)
        .description('Входной X порт'),
      new Port('y').output().data('number')
        .description('Результат Y')
    ]
  }

  checkParams () {
    return [
      new Rule('countFromZero').required().default(false).isBoolean()
        .description('Если `true` биты будут считатся от 0 (0 = первый бит)'),
      new Rule('default').required().default([]).isArray()
        .description('Значение по умолчанию для кажого входа (0 индекс = 1 вход)')
    ]
  }

  settings () {
    return {
      message: false,
      shares: false
    }
  }

  preProcess () {
    for (let i = 0; i < 2; i++) {
      this.xs[i] = (this.params.default[i] !== undefined) ? this.params.default[i] : 0
      this[`inputX${i + 1}`] = (data) => {
        if (this.xs[i] === data) return
        this.xs[i] = data
        var res = 0
        if (this.params.countFromZero) res = (this.xs[0] >> this.xs[1]) & 1
        else res = (this.xs[0] >> (this.xs[1] - 1)) & 1
        this.outputs['y' + (i + 1)].push(res)
      }
    }
  }

  xs = []
}
