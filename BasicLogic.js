
const { Port, Rule, Device } = require('vrack-core')
module.exports = class extends Device {
  ports () {
    return [
      new Port('x%d').input().data('number')
        .dynamic(this.params.inputs)
        .description('Входной X порт'),
      new Port('y').output().data('number')
        .description('Результат Y')
    ]
  }

  checkParams () {
    return [
      new Rule('result').required().enum(['logic', 'boolean', 'number']).default('number').isString()
        .description('Постобработка результата `logic` (y = y & 1), `boolean` (if (y) y = 1 else y = 0), `number` (y = y)'),
      new Rule('inputs').required().default(2).isInteger().expression('value > 0')
        .description('Количество входов'),
      new Rule('startSignal').required().default(false).isBoolean()
        .description('Подавать ли стартовый сигнал после создания устройства (не рекомендуется)'),
      new Rule('default').required().default([]).isArray()
        .description('Значение по умолчанию для кажого входа (0 индекс = 1 вход)')
    ]
  }

  preProcess () {
    for (let i = 0; i < this.params.inputs; i++) {
      this.xs[i] = (this.params.default[i] !== undefined) ? this.params.default[i] : 0
      this[`inputX${i + 1}`] = (data) => {
        this.xs[i] = data
        this.outputY()
      }
    }
  }

  settings () {
    return {
      message: false,
      shares: false
    }
  }

  xs = []
  y = 0

  process () {
    if (this.params.startSignal) this.outputY()
  }

  outputY () {
    var y = this.xs.reduce(this.countY)
    switch (this.params.result) {
      case 'boolean': if (y) y = 1; else y = 0; break
      case 'logic': y &= 1; break
    }
    if (y !== this.y) {
      this.y = y
      this.outputs.y.push(this.y)
    }
  }
}
