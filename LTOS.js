
const { Port, Rule, Device } = require('vrack-core')
module.exports = class extends Device {
  ports () {
    return [
      new Port('unit%d').input().data('number').dynamic(this.params.ports)
        .description('Логический вход'),
      new Port('signal%d').output().data('signal').dynamic(this.params.ports)
        .description('Выходной сигнал')
    ]
  }

  checkParams () {
    return [
      new Rule('ports').required().default(1).isInteger().expression('value >= 1 && value <= 32')
        .description('Количество портов - адаптеров'),
      new Rule('timeout').required().default(100).isInteger().expression('value > 0')
        .description('Таймаут после которого будет доступна повторная отправка сигнала')
    ]
  }

  settings () {
    return {
      message: false,
      shares: false
    }
  }

  portsTimeout = {}

  preProcess () {
    for (let i = 1; i <= this.params.ports; i++) {
      this['inputUnit' + i] = (data) => {
        if (this.portsTimeout[i]) return
        if (data) this.outputs['signal' + i].push(data)
        this.portsTimeout[i] = setTimeout(() => {
          this.portsTimeout[i] = false
        }, this.params.timeout)
      }
    }
  }
}
