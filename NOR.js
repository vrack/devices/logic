const Logic = require('./BasicLogic')
module.exports = class extends Logic {
  description = 'Логическое "ИЛИ-НЕ". Результ:  y = ~(x1 | x2)'
  countY (c, n) { return ~(c | n) }
}
