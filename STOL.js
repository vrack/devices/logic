const { Port, Rule, Device } = require('vrack-core')
module.exports = class extends Device {
  description = '(Signal to logic adapter) Простой преобразователь сигнала в 0 и 1'

  ports () {
    return [
      new Port('signal%d').input().data('signal').dynamic(this.params.ports)
        .description('Логический вход'),
      new Port('unit%d').output().data('number').dynamic(this.params.ports)
        .description('Выходной сигнал')
    ]
  }

  checkParams () {
    return [
      new Rule('ports').required().default(1).isInteger().expression('value >= 1 && value <= 32')
        .description('Количество портов - адаптеров'),
      new Rule('timeout').required().default(100).isInteger().expression('value > 0')
        .description('Количество мс на которое будет подана единица на выход, после выхода таймаута, на выход будет подан ноль')
    ]
  }

  settings () {
    return {
      message: false,
      shares: false
    }
  }

  portsTimeout = {}

  preProcess () {
    for (let i = 1; i <= this.params.ports; i++) {
      this['inputSignal' + i] = () => {
        if (this.portsTimeout[i]) return
        this.outputs['unit' + i].push(1)
        this.portsTimeout[i] = setTimeout(() => {
          this.portsTimeout[i] = false
          this.outputs['unit' + i].push(0)
        }, this.params.timeout)
      }
    }
  }
}
