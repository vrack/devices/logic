const Logic = require('./BasicLogic')
module.exports = class extends Logic {
  description = 'Логическое "И-НЕ". Результ:  y = ~(x1 & x2)'
  countY (c, n) { return ~(c & n) }
}
