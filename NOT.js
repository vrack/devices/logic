const { Port, Rule, Device } = require('vrack-core')
module.exports = class extends Device {
  description = 'Логическое "НЕ". Результ: y1 = (~x1) & 1'

  ports () {
    return [
      new Port('x%d').input().data('number')
        .dynamic(this.params.ports)
        .description('Входной X порт'),
      new Port('y%d').output().data('number')
        .dynamic(this.params.ports)
        .description('Результат Y')
    ]
  }

  checkParams () {
    return [
      new Rule('result').required().enum(['logic', 'boolean', 'number']).default('number').isString()
        .description('Постобработка результата `logic` (y = y & 1), `boolean` (if (y) y = 1 else y = 0), `number` (y = y)'),
      new Rule('ports').required().default(1).isInteger().expression('value > 0')
    ]
  }

  settings () {
    return {
      message: false,
      shares: false
    }
  }

  preProcess () {
    for (let i = 0, p = 1; i < this.params.ports; i++, p++) {
      this.xs[i] = 0
      this[`inputX${p}`] = (data) => {
        if (this.xs[i] !== data) {
          this.xs[i] = data
          this.outputY(~data, p)
        }
      }
    }
  }

  xs = []

  outputY (y, p) {
    switch (this.params.result) {
      case 'boolean': if (y) y = 1; else y = 0; break
      case 'logic': y &= 1; break
    }
    this.outputs['y' + p].push(y)
  }
}
