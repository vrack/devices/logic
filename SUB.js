const Logic = require('./BasicLogic')
module.exports = class extends Logic {
  description = 'Операция вычитания. Результ: y = x1 - x2 - xN'
  countY (c, n) { return c - n }
}
